﻿using CSharpExercises.Exercise1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace UnitTests
{
    public class Ex1Tests
    {
        [Fact]
        public void FactorialTest()
        {
            Ex1 instanceUnderTest = new Ex1();

            Assert.Equal(1, instanceUnderTest.Factorial(0));
            Assert.Equal(1, instanceUnderTest.Factorial(1));
            Assert.Equal(2, instanceUnderTest.Factorial(2));
            Assert.Equal(6, instanceUnderTest.Factorial(3));
            Assert.Equal(120, instanceUnderTest.Factorial(5));
            Assert.Equal(362880, instanceUnderTest.Factorial(9));
        }    
        
        [Fact]
        public void FibonacciTest()
        {
            Ex1 instanceUnderTest = new Ex1();

            Assert.Equal(0, instanceUnderTest.Fibonacci(0));
            Assert.Equal(1, instanceUnderTest.Fibonacci(2));
            Assert.Equal(2, instanceUnderTest.Fibonacci(3));
            Assert.Equal(89, instanceUnderTest.Fibonacci(11));
            Assert.Equal(4181, instanceUnderTest.Fibonacci(19));
        } 
    }
}
